﻿using System;
using System.Windows.Forms;

namespace lekcja_5._operatory_mat_i_logiczne_roz
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOblicz_Click(object sender, EventArgs e)
        {
            int droga = int.Parse(txtDroga.Text);
            int czas = int.Parse(txtCzas.Text);

            float predkosc = (float)droga / czas;
            txtWynik.Text = predkosc.ToString();
        }
    }
}
